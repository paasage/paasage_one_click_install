#!/bin/bash
. $HOME/override_vars.sh


sudo usermod libuuid --shell /usr/sbin/nologin

# prevent private stuff being stored in template
rm $HOME/override_vars.sh

TODAY=$(date "+%Y-%m-%d")
cd $HOME
curl --insecure -O -O https://factory.apphub.eu.com/resources/uforge-scan.bin
chmod +x ./uforge-scan.bin 
sudo ./uforge-scan.bin -u $APPHUB_USERNAME -p $APPHUB_API_KEY -U https://factory.apphub.eu.com/api -n "PAASAGE-REVIEW-$TODAY"
