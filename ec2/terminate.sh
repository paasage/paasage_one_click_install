#!/bin/bash
#set -x
. ./parameters
OVER_FILE=$1
. ./parameters
if [ -f "$OVER_FILE" ]
then
  . $OVER_FILE
fi
DRY=--dry-run
DRY=
INSTANCE_ID=$(aws ec2 describe-instances --filter Name=tag:Name,Values=PAASAGE_DEVELOP --query Reservations[].Instances[].InstanceId | tr -d '"[]' | tr "," " " | tr -d '\n' )
echo $INSTANCE_ID

aws ec2 terminate-instances --instance-ids $INSTANCE_ID
