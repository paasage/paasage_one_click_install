#!/bin/bash
OVER_FILE=$1
. ./parameters
#set -x
if [ -f "$OVER_FILE" ]
then
  . $OVER_FILE
fi
DRY=--dry-run
DRY=
INSTANCE_ID=$(aws ec2 run-instances $DRY \
     --image-id $AMI  \
     --security-group-ids $SECURITY_GROUP \
     --count 1 \
     --instance-type $INSTANCE_TYPE \
     --key-name $KEY_NAME \
     --subnet-id  $SUBNET_ID \
     --user-data file://cloud-init.yml  \
     --block-device-mapping 'DeviceName=/dev/sda1,Ebs={VolumeSize=33}' \
     --query 'Instances[0].InstanceId' |  tr -d '"')
sleep 1
aws ec2 create-tags $DRY  --resources=$INSTANCE_ID --tag "Key=Name,Value=$NAME"
sleep 1

while true
do
  STATE=$(aws ec2 describe-instances --instance-id  $INSTANCE_ID --query Reservations[0].Instances[0].State.Name | tr -d '"[]' | tr "," " " | tr -d '\n' )
  echo $STATE
  if [ "z$STATE" == "zrunning" ] 
  then
    break
  fi
  sleep 5
done

aws ec2 associate-address $DRY --instance-id=$INSTANCE_ID  --allocation-id=$ALLOCATION_ID

