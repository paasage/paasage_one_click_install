

1. PaaSage Platform Configuration & Deployment 

A lot of effort has been put into the automation of configuration and deployment of the PaaSage platform to make it as easy as possible for a newcomer to get it running. 

1.1	Supported platforms & Resource Requirements
PaaSage is only supported on the Linux platform. Although any modern Linux distribution can be exploited, the PaaSage platform deployment has only been tested on Ubuntu 14.04 64bits.
PaaSage needs to be deployed on a virtual machine with at least
•	2 CPU cores
•	8 GB ram
•	20 GB Hard disk

During application deployment by PaaSage, the destinations virtual machines need to “phone home” and contact the PaaSage platform (EG: to report deployment progress or for metrics collection). 
The PaaSage platform thus needs to be reachable on the following ports

•	22/TCP: 	SSH 
•	80/TCP:  	execution ware UI
•	4001/TCP:	colosseum etcd daemon
•	8080/TCP: 	time series database
•	9000/TCP: 	colosseum UI
•	9999/TCP: 	REST API
•	33034/TCP: 	RMI registry

1.2	Deployment of the PaaSage platform

As already stated, a lot of effort has been put to hide the complexity of the PaaSage platform deployment. 
A few simple steps to execute on a fresh virtual machine should get you started.
In the following procedures, we assume the PaaSage platform is deployed on a Remote Cloud and not on a local workstation.

1.2.1	STEP #1: VM CREATION
•	Actual actions depend on the Cloud provider (EC2, AZURE, OPENSTACK instance)
•	Please make sure the requirements are fulfilled
•	VM size
•	PUBLIC_IP MAPPING
•	PORTS accessible
•	SSH access

1.2.2	STEP #2: Bootstrap code download
•	Log into the PaaSage VM
•	Run the following commands
$ sudo apt-get update
$ sudo apt-get install git
$ git clone https://tuleap.ow2.org/plugins/git/paasage/
paasage_one_click_install.git 
•	Bootstrap code is now downloaded on the to-be PaaSage VM.

1.2.3	STEP #3 Bootstrap code configuration
The bootstrap code is tailored by a few shell script variables. These variables need to be adapted for each deployment of the PaaSage platform.
The configuration file must be named $HOME/override_vars.sh. An example is provided together with the bootstrap script. 
Please follow these steps to configure the bootstrap script.
•	Log into PaaSage VM
•	Run the following commands
 $ cp paasage_one_click_install/override_vars.sh_SAMPLE	./override_vars.sh
  $ vi override_vars.sh
•	Adapt the file with your values 
o	NODE_GROUP: identify the vm’s created by this PaaSage platform (use only lowercase letters and limit to 10 characters)
o	ELASTIC_IP:  public IP address assigned to this PaaSage Platform (by default, detected using checkip.amazonaws.com)
o	SEED: some random string (lower case letters) used as seed for password generation
o	Destination clouds credentials 
1.2.4	STEP #4 Bootstrap code execution
Once the override_vars.sh is adapted, you can run the PaaSage platform deployment.
•	Log into the PaaSage platform, then execute
$ paasage_one_click_install/bootstrap/bootstrap.sh
•	Watch the deployment taking place. It can take from 10 to 20 min
1.2.5	STEP #5: Connect Social Network to the new PaaSage Platform
Connecting the PaaSage platform with the social network (SN) allows the latter to be used for CAMEL model deployment.
•	Connect to the Social Network web site 
•	Navigate to My Area  Credentials 
•	Fill in the endpoint, email, password and tenant
•	Click on the Save changes button
1.2.6	STEP #6 Clean up
If you want to reuse this PaaSage platform to deploy another application, you need to clean the databases up
•	Log into the PaaSage Platform
•	Execute the following command
$ /etc/paasage/clean-db.sh 
•	Reboot the platform



