#!/bin/bash
# PaaSage Platform parameters
export CHEF_VERSION=12.3.0
export BERKSHELF_VERSION=4.3.5
export DEPLOYMENT_SCRIPTS_GIT_REPOSITORY=https://tuleap.ow2.org/plugins/git/paasage/deployment_scripts.git
# variable to override in $HOME/override_vars.sh
export DEPLOYMENT_SCRIPTS_BRANCH=zeromq
# PaaSage VM Parameters
export ELASTIC_IP=$(curl -X GET http://checkip.amazonaws.com/)
#echo "TEST: ELASTIC_IP as it would be give by http://checkip.amazonaws.com/ : $ELASTIC_IP"
#export ELASTIC_IP=$(ifconfig eth0 | grep 'inet ad' |  cut --delimiter=: -f 2 | cut -f 1 --delimiter=\ )
# seed for password generation
export SEED=reotzetouzrto
# Flexiant Credentials
FLEXIANT_USERNAME=toto
FLEXIANT_PASSWORD=tata
FLEXIANT_ENDPOINT=optional
# OMISTACK credentials
OMISTACK_USERNAME=titi
OMISTACK_PASSWORD=tutu
OMISTACK_ENDPOINT=optional
# GWDG Credentials
GWDG_USERNAME=tuutuu
GWDG_PASSWORD=tootoo
GWDG_ENDPOINT=https://api.cloud.gwdg.de:5000/v2.0/
# EC2 Credentials
EC2_USERNAME=tatata
EC2_PASSWORD=tototo
EC2_ENDPOINT=https://ec2.eu-west-1.amazonaws.com



STEP=$1
if [ "z"  ==  "z$STEP" ]
then
  STEP="ALL"
fi


chmod +x "$HOME/override_vars.sh"

if [ -x "$HOME/override_vars.sh" ] ;
then
. $HOME/override_vars.sh
fi


# bootstrap script to install PaaSage on a freshly created vm

function install_packages()
{
  export DEBIAN_FRONTEND=noninteractive
  sudo apt-get update
  sudo apt-get dist-upgrade -y
  sudo apt-get install -y git build-essential curl haveged
}

function install_chef()
{
   if [ ! -f  "/opt/chef/bin/chef-solo" ]
   then   
     curl -L https://omnitruck.chef.io/install.sh | sudo bash -s --  -v $CHEF_VERSION
     sudo /opt/chef/embedded/bin/gem install --no-ri --no-rdoc berkshelf  -v  $BERKSHELF_VERSION
   fi
}


function clone_repository()
{
  cd $HOME
  rm -rf deployment_scripts
  git clone $DEPLOYMENT_SCRIPTS_GIT_REPOSITORY
  cd deployment_scripts
  git checkout $DEPLOYMENT_SCRIPTS_BRANCH
  /opt/chef/embedded/bin/berks install
  /opt/chef/embedded/bin/berks vendor
}


function prepare_solo_rb()
{
cat >$HOME/solo.rb <<EOF
root = File.absolute_path(File.dirname(__FILE__))

file_cache_path root
cookbook_path '$HOME/deployment_scripts/berks-cookbooks/'
EOF


}



function prepare_node_json()
{

cat >$HOME/node.json <<EOF
{
  "run_list": [
    "role[paasage]"
  ],
  "default": {
    "apt": {
      "cacher_ipaddress": "",
      "cacher_interface": "",
      "cacher_port": ""
    }
  },
  
  "paasage": {
    "installation_user": "$USER"

  },
  "play_app": {
    "dbHost": "%",
    "dbPass": "ttzeirtoezr$SEED",
    "external_ip_address": "$ELASTIC_IP",
    "node_group": "$NODE_GROUP",
    "application_secret_key": "qjkdjklgsdfgjsdfjgjsdfgjkdsfg$SEED"
  },
  "mysql" : {
    "server_root_password": "zekljazerajer$SEED"
  },
  "webmasterscript": {
    "git_username":"paasage_demo",
    "git_password": "PaaSageReview"
  },
  "execware":{
    "user_interface": {
      "listen_ip_address": "$ELASTIC_IP"
    }
  },
  "metabase":{
    "cdo_server": {
      "dbPass": "467898765dfqd_$SEED"
    }
  },
  "webmasterscript": {
    "branch": "master"
  }
}
EOF


}

function deploy_paasage()
{
  sudo chef-solo -c $HOME/solo.rb -j $HOME/node.json
}

function preprare_credentials()
{
cat>/tmp/credentials <<EOF
Flexiant-uname=$FLEXIANT_USERNAME
Flexiant-pass=$FLEXIANT_PASSWORD
Flexiant-endpoint=$FLEXIANT_ENDPOINT
#
Omistack-uname=$OMISTACK_USERNAME
Omistack-pass=$OMISTACK_PASSWORD
Omistack-endpoint=$OMISTACK_ENDPOINT
#
GWDG-uname=$GWDG_USERNAME
GWDG-pass=$GWDG_PASSWORD
GWDG-endpoint=$GWDG_ENDPOINT
#
EC2-uname=$EC2_USERNAME
EC2-pass=$EC2_PASSWORD
EC2-endpoint=$EC2_ENDPOINT
EOF

sudo mkdir -p /etc/paasage
sudo chmod 757 /etc/paasage
cp  /tmp/credentials  /etc/paasage/eu.paasage.upperware.cloudcredentials.properties
}

# 1. Install packages
if [ "zALL" == "z$STEP" -o "zSTEP_1" == "z$STEP" ]
then
 install_packages
fi 
# 2. Install chef

if [ "zALL" == "z$STEP" -o "zSTEP_2" == "z$STEP" ]
then
install_chef
fi
# 3. clone repository
if [ "zALL" == "z$STEP" -o "zSTEP_3" == "z$STEP" ]
then
clone_repository
fi
# 4. prepare files
if [ "zALL" == "z$STEP" -o "zSTEP_4" == "z$STEP" ]
then
prepare_solo_rb
prepare_node_json
echo "############### create cloud provider credentials"
#preprare_credentials
fi
# 5. deploy paasage
if [ "zALL" == "z$STEP" -o "zSTEP_5" == "z$STEP" ]
then
deploy_paasage
fi
# 6. Prepare swap file
if [ "zALL" == "z$STEP" -o "zSTEP_6" == "z$STEP" ]
then
echo $HOME/paasage_one_click_install/bootstrap/create_swap_file.sh 
fi
# 7. clone execware tester
if [ "zALL" == "z$STEP" -o "zSTEP_7" == "z$STEP" ]
then
pushd $HOME
git clone https://tuleap.ow2.org/plugins/git/paasage/paasage_rest_tester.git
popd
fi

