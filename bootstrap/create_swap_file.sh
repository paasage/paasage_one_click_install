#!/bin/bash

SIZE=4096
echo "preparing swap file"
sudo dd if=/dev/zero of=/swapfile1 bs=1M count=$SIZE
sudo chown root:root /swapfile1
sudo chmod 0600 /swapfile1
sudo mkswap /swapfile1
sudo swapon /swapfile1
cat >/tmp/fs <<EOF
/swapfile1 none swap sw 0 0
EOF

grep swapfile1 /etc/fstab
RETVAL=$?
if [ $RETVAL -eq 1 ] 
then
cat /etc/fstab /tmp/fs >/tmp/fstab.new
sudo  mv /tmp/fstab.new /tmp/fstab

fi 

