#!/bin/bash
AWS=$HOME/bin/aws
CURDIR="$( cd "$( dirname "$0" )" && pwd )"
pushd $CURDIR
#set -x
. ./parameters
OVER_FILE=$1
. ./parameters
if [ -f "$OVER_FILE" ]
then
  . $OVER_FILE
fi
DRY=--dry-run
DRY=
INSTANCE_ID=$($AWS ec2 describe-instances --filter Name=tag:Name,Values=$NAME --query Reservations[].Instances[].InstanceId | tr -d '"[]' | tr "," " " | tr -d '\n' )
echo $INSTANCE_ID

$AWS ec2 terminate-instances --instance-ids $INSTANCE_ID
while true
do
  STATE=$($AWS ec2 describe-instances --instance-id  $INSTANCE_ID --query Reservations[0].Instances[0].State.Name | tr -d '"[]' | tr "," " " | tr -d '\n' )
  echo $STATE
  if [ "z$STATE" == "zterminated" ] 
  then
    break
  fi
  sleep 5
done
popd

